namespace my.bookshop;

entity Books {
  key ID : Integer;
  title  : String;
  stock  : Integer;
}

entity Orders {
  key OrderID : Integer;
  OrderDate : DateTime default $now;
  ExpDate: DateTime default $now;
  UserName : String;
  CustomerID : String;
  CompanyName : String;
  Items :  Composition of  many OrdersItems on Items.Order = OrderID;
}

entity OrdersItems {
  key Order : Integer;
  key ProductID: Integer;
  ProductName : String;
  QuantityPerUnit : String;
  Quantity : Integer;
  UnitPrice : Integer;

}

entity Sample {
  UserName : String;
  FirstName : String;
  LastName : String;
  Age : Integer;

}