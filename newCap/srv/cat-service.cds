using my.bookshop as my from '../db/data-model';

service CatalogService @(path: 'CatalogService', requires:'any'){
    entity Books @(restrict:[
            {
                grant:['READ'],
                to   :['booksRead']
            },
            {
                grant:['CREATE'],
                to   :['booksCreate']
            },
            {
                grant:['UPDATE'],
                to   :['booksUpdate']
            },
            {
                grant:['DELETE'],
                to   :['booksDelete']
            }
        ]
    )
    as projection on my.Books;
    
    entity Orders @(restrict:[
            {
                // dovrebbe essere così: grant:['READ'],
                grant:['*'],
                to   :['orderViewer']
            },
            {
                // grant:['READ','WRITE'] è la stessa cosa di quanto messo 
                grant:['*'],
                to   :['orderAdmin']
            }
        ]
    )
    as projection on my.Orders;

    entity OrdersItems @(restrict:[
            {
                // dovrebbe essere così: grant:['READ'],
                grant:['*'],
                to   :['orderViewer']
            },
            {
                // grant:['READ','WRITE'] è la stessa cosa di quanto messo 
                grant:['*'],
                to   :['orderAdmin']
            }
        ]
    )
    as projection on my.OrdersItems;

    entity Sample @(restrict:[
            {
                // dovrebbe essere così: grant:['READ'],
                grant:['*'],
                to   :['orderViewer']
            },
            {
                // grant:['READ','WRITE'] è la stessa cosa di quanto messo 
                grant:['*'],
                to   :['orderAdmin']
            }
        ]
    )
    as projection on my.Sample;
}

service PublicCatalogService @(path: 'PublicCatalogService', requires:'any'){
    @readonly entity Books as projection on my.Books;
    @readonly entity Orders as projection on my.Orders;
    @readonly entity OrdersItems as projection on my.OrdersItems;
}

