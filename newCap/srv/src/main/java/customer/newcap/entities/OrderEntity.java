package customer.newcap.entities;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import cds.gen.my.bookshop.Orders;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderEntity implements Serializable{
        @JsonProperty(Orders.ORDER_ID)
        private int OrderID;
        @JsonProperty(Orders.EXP_DATE)
        private String ExpDate;
        @JsonProperty(Orders.USER_NAME)
        private String UserName;
        @JsonProperty(Orders.CUSTOMER_ID)
        private String CustomerID;
        @JsonProperty(Orders.COMPANY_NAME)
        private String CompanyName;
        @JsonProperty(Orders.ITEMS)
        private List<ProductEntity> Items; 

        public String toString() {
                return "OrderEntity [CompanyName-" + CompanyName + ", CustomerID-" + CustomerID + 
                         ", UserName" + UserName + ", Items" + Items + ", OrderID" + OrderID + "]";
        }

}
