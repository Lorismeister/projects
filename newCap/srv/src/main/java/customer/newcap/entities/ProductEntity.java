package customer.newcap.entities;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import cds.gen.publiccatalogservice.OrdersItems;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductEntity implements Serializable{
    @JsonProperty(OrdersItems.ORDER)
    private int Order;
    @JsonProperty(OrdersItems.PRODUCT_ID)
    private int ProductID;
    @JsonProperty(OrdersItems.PRODUCT_NAME)
    private String ProductName;
    @JsonProperty(OrdersItems.QUANTITY_PER_UNIT)
    private String QuantityPerUnit;
    @JsonProperty(OrdersItems.UNIT_PRICE)
    private String UnitPrice;
    @JsonProperty(OrdersItems.QUANTITY)
    private String Quantity;   
    
}
