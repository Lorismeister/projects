package customer.newcap.util;

public class PrjConstants {
    public final static String BOOKS_CREATE = "booksCreate";
    public final static String BOOKS_READ = "booksRead";
    public final static String BOOKS_UPDATE = "booksUpdate";
    public final static String BOOKS_DELETE = "booksDelete";
    //ORDERS 
    public final static String AUTH_ORDERS_RO = "orderViewer";
    public final static String AUTH_ORDERS_RW = "orderAdmin";
}
