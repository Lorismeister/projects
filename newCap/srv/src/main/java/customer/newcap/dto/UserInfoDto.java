package customer.newcap.dto;

import java.util.List;

public class UserInfoDto {  //Se appaiono in giallo clicca sulla lampadina e prendi i suggerimenti
    private String name;
    private String surname;
    private String email;
    private List<String> authorizations;
    
    public UserInfoDto(String name, String surname, String email, List<String> authorizations) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.authorizations = authorizations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getAuthorizations() {
        return authorizations;
    }

    public void setAuthorizations(List<String> authorizations) {
        this.authorizations = authorizations;
    }

}
