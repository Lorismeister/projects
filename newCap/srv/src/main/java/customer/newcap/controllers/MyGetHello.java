package customer.newcap.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import customer.newcap.services.MyGetSrv;

@RestController
public class MyGetHello extends MyEndpoint {
    @Autowired
    MyGetSrv myGetSrv;

    @GetMapping(value = "get")
    public ResponseEntity<String> getHello() {
        return ResponseEntity.ok(myGetSrv.getHelloString());
    }
}
