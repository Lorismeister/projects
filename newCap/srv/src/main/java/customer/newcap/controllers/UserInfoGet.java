package customer.newcap.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import customer.newcap.dto.UserInfoDto;
import customer.newcap.services.MyAuthSrv;
import customer.newcap.services.UserInfoSrv;

@RestController
public class UserInfoGet extends MyEndpoint{
    @Autowired
    UserInfoSrv userInfoSrv;

    @Autowired
    MyAuthSrv authSrv;

    @GetMapping(value="getUserInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserInfoDto> getUserInfo() {
        return ResponseEntity.ok(new UserInfoDto(userInfoSrv.getName(),userInfoSrv.getSurname(),userInfoSrv.getEmail(),authSrv.getAutorization()));
    }
}
