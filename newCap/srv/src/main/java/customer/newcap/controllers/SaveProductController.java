package customer.newcap.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import customer.newcap.entities.OrderEntity;
import customer.newcap.services.MyOrderSrv;

@RestController
public class SaveProductController extends MyEndpoint{

    private final MyOrderSrv myOrderSrv;

    @Autowired
    public SaveProductController(MyOrderSrv myOrderSrv) {
        this.myOrderSrv = myOrderSrv;
    }

    @PostMapping(value = "/saveOrder", consumes = MediaType.APPLICATION_JSON_VALUE)
        public ResponseEntity<String> salvaOrdine(@RequestBody OrderEntity order) {
            try {myOrderSrv.saveOrder(order); }
            catch (Exception e) {
                return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
            }
            return new ResponseEntity<>(order.toString(), HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteOrder", consumes = MediaType.APPLICATION_JSON_VALUE)
        public ResponseEntity<String> eliminaOrdine(@RequestBody OrderEntity order) {
            try {myOrderSrv.deleteOrder(order); }
            catch (Exception e) {
                return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
            }
            return new ResponseEntity<>(order.toString(), HttpStatus.OK);
    }

    @PutMapping(value = "/updateOrder", produces = MediaType.APPLICATION_JSON_VALUE)
        public ResponseEntity<String> updateOrder(@RequestBody OrderEntity order) {
            try {myOrderSrv.updateOrder(order); }
            catch (Exception e) {
                return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
            }
            return new ResponseEntity<>(order.toString(), HttpStatus.OK);
    }
    
}
