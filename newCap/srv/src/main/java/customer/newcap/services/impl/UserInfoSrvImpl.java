package customer.newcap.services.impl;

import org.springframework.stereotype.Service;

import com.sap.cloud.sdk.cloudplatform.security.AuthTokenAccessor;

import customer.newcap.services.UserInfoSrv;

@Service
public class UserInfoSrvImpl implements UserInfoSrv{
    @Override
    public String getName() {
        return getClaim("given_name");
    }

    @Override
    public String getSurname() {
        return getClaim("family_name");
    }

    @Override
    public String getEmail() {
        return getClaim("email");
    }

    private String getClaim(final String propertyName){
        return AuthTokenAccessor.getCurrentToken().getJwt().getClaim(propertyName).toString();
    }
}
