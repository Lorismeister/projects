package customer.newcap.services;

public interface UserInfoSrv {
    String getName();
    String getSurname();
    String getEmail();
}