package customer.newcap.services.impl;

import org.springframework.stereotype.Service;

import customer.newcap.services.MyGetSrv;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MyGetSrvImpl implements MyGetSrv{

    @Override
    public String getHelloString() {
        //log.debug("Called method getHelloString()");
        return "Hello World";
    }
    
}
