package customer.newcap.services.impl;

import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.lang.model.element.QualifiedNameable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sap.cds.ql.Delete;
import com.sap.cds.ql.Update;
import com.sap.cds.ql.Upsert;
import com.sap.cds.ql.cqn.CqnDelete;
import com.sap.cds.ql.cqn.CqnUpdate;
import com.sap.cds.ql.cqn.CqnUpsert;
import com.sap.cds.services.persistence.PersistenceService;

import customer.newcap.entities.OrderEntity;
import customer.newcap.entities.ProductEntity;
import customer.newcap.services.MyOrderSrv;

@Service
public class MyOrderSrvImp implements MyOrderSrv{

    @Autowired
    PersistenceService db;

    @Override
    public void saveOrder(OrderEntity order) {
        Map<String, Object> newOrder = new HashMap<>();
        newOrder.put("OrderID", order.getOrderID());
        newOrder.put("ExpDate", order.getExpDate());
        newOrder.put("UserName", order.getUserName());
        newOrder.put("CustomerID", order.getCustomerID());
        newOrder.put("CompanyName", order.getCompanyName());
        List<Map<String,Object>> ordersItem = new LinkedList<>();

        for(ProductEntity ordersItems : order.getItems()) {
            Map<String, Object> newOrdersItem = new HashMap<>();
            newOrdersItem.put("ProductID", ordersItems.getProductID());
            newOrdersItem.put("Order", order.getOrderID());
            newOrdersItem.put("ProductName", ordersItems.getProductName());
            newOrdersItem.put("QuantityPerUnit", ordersItems.getQuantityPerUnit());
            newOrdersItem.put("Quantity", ordersItems.getQuantity());
            newOrdersItem.put("UnitPrice", ordersItems.getUnitPrice());
            ordersItem.add(newOrdersItem);
     
        }
        
        newOrder.put("Items", ordersItem);
        
        CqnUpsert insert = Upsert.into("CatalogService.Orders").entry(newOrder);
            db.run(insert);
    }

    @Override
    public void deleteOrder(OrderEntity order) {
                
        CqnDelete delete = Delete.from("CatalogService.Orders").byId(order.getOrderID());
            db.run(delete);
    }
    
    @Override
    public void updateOrder(OrderEntity order) {
        Map<String, Object> updateOrder = new HashMap<>();
        updateOrder.put("OrderID", order.getOrderID());
        updateOrder.put("ExpDate", order.getExpDate());
        updateOrder.put("UserName", order.getUserName());
        updateOrder.put("CustomerID", order.getCustomerID());
        updateOrder.put("CompanyName", order.getCompanyName());
        List<Map<String,Object>> updatedItem = new LinkedList<>();

        for(ProductEntity ordersItems : order.getItems()) {
            Map<String, Object> updateOrdersItem = new HashMap<>();
            updateOrdersItem.put("ProductID", ordersItems.getProductID());
            updateOrdersItem.put("Order", order.getOrderID());
            updateOrdersItem.put("ProductName", ordersItems.getProductName());
            updateOrdersItem.put("QuantityPerUnit", ordersItems.getQuantityPerUnit());
            updateOrdersItem.put("Quantity", ordersItems.getQuantity());
            updateOrdersItem.put("UnitPrice", ordersItems.getUnitPrice());
            updatedItem.add(updateOrdersItem);
     
        }
        
        updateOrder.put("Items", updatedItem);
        
        CqnUpdate update = Update.entity("CatalogService.Orders").data(updateOrder).byId(order.getOrderID());
            db.run(update);
    }
    
}
