package customer.newcap.services;

import customer.newcap.entities.OrderEntity;

public interface MyOrderSrv {
    
    void saveOrder(OrderEntity order);
    void deleteOrder(OrderEntity order);
    void updateOrder(OrderEntity order);
}
