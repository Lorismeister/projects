package customer.newcap.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.sap.cloud.sdk.cloudplatform.security.principal.Principal;
import com.sap.cloud.sdk.cloudplatform.security.principal.PrincipalAccessor;

import customer.newcap.services.MyAuthSrv;
import customer.newcap.util.PrjConstants;

@Service
public class MyAuthSrvImpl implements MyAuthSrv{

    @Override
    public List<String> getAutorization() {
        // TODO Auto-generated method stub
        return getAuthList();
    }

    private List<String> getAuthList() {
        List<String> userAuth = new ArrayList<>();
        
        //Strumento per accedere alle info del jwt
        Principal pa = PrincipalAccessor.getCurrentPrincipal();
        String authString = pa.getAuthorizations().toString();
        if (authString.contains(PrjConstants.BOOKS_CREATE)){
            userAuth.add(PrjConstants.BOOKS_CREATE);
        }
        if (authString.contains(PrjConstants.BOOKS_READ)){
            userAuth.add(PrjConstants.BOOKS_READ);
        }
        if (authString.contains(PrjConstants.BOOKS_UPDATE)){
            userAuth.add(PrjConstants.BOOKS_UPDATE);
        }
        if (authString.contains(PrjConstants.BOOKS_DELETE)){
            userAuth.add(PrjConstants.BOOKS_DELETE);
        }

        // AUTH ORDERS 

        if (authString.contains(PrjConstants.AUTH_ORDERS_RO)){
            userAuth.add(PrjConstants.AUTH_ORDERS_RO);
        }
        if (authString.contains(PrjConstants.AUTH_ORDERS_RW)){
            userAuth.add(PrjConstants.AUTH_ORDERS_RW);
        }

        return userAuth;
    }
}
