/* global QUnit */

sap.ui.require(["customer/app1/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
