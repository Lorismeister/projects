sap.ui.define([
    "./BaseController",
    "../model/ServiceAPI",
    "sap/m/MessageBox",
    'sap/ui/core/Fragment'
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, ServiceAPI, MessageBox, Fragment) {
        "use strict";

        return Controller.extend("customer.app1.controller.OrderEdit2", {

            counter: 0,

            onInit: function () {

                // @ts-ignore
                this._ServiceAPI = new ServiceAPI(this);

                // @ts-ignore
                this.datiOrdersModel2 = new sap.ui.model.json.JSONModel();
                // @ts-ignore
                this.getView().setModel(this.datiOrdersModel2, "datiOrdersModel2");

                // @ts-ignore
                this.productsModel = new sap.ui.model.json.JSONModel();
                // @ts-ignore
                this.getView().setModel(this.productsModel, "productsModel");

                // @ts-ignore
                this.orderDetailsModel2 = new sap.ui.model.json.JSONModel();
                // @ts-ignore
                this.getView().setModel(this.orderDetailsModel2, "orderDetailsModel2");

                // @ts-ignore
                this.getRouter().getRoute("RouteOrderEdit").attachPatternMatched(this._handleEditMatched, this);
            },

            _handleEditMatched: function (evt) {
                var query3 = evt.getParameter("arguments")["?query"];
                if (query3 && query3.ID) {
                    this.readOrders(query3.ID)
                }

                this.readProducts();
            },

            readOrders: async function (ID) {
                // @ts-ignore
                var jurl = "odata/v4/OrderService/Orders(" + ID + ")?$expand=Items";

                // @ts-ignore
                return new Promise(function (resolve, reject) {
                    // @ts-ignore
                    $.ajax({
                        method: "GET",
                        url: jurl,
                        async: true,
                    }).done(function (data) {
                        this.datiOrdersModel2.setData(data);
                        this.orderDetailsModel2.setData(data.Items);
                        resolve();
                        // @ts-ignore
                    // @ts-ignore
                    }.bind(this)).fail(function (jqHXR, textStatus) {
                        MessageBox.error(jqHXR.responseText);
                        reject();
                    });
                }.bind(this));
            },

            onNavBack: function () {
                // @ts-ignore
                this.goToRoute("RouteView1")
            },

            onQuantityChange: function (oEvent) {
                var inputValue = oEvent.getParameter("value");
                var intValue = parseInt(inputValue, 10); // Converte il valore in un numero intero

                // Salva il valore nell'oggetto del controller
                // @ts-ignore
                this.quantityValue = intValue;
            },

            onUpdateOrderPress: function () {
                var orderId = this.getView().getModel("datiOrdersModel2").getProperty("/OrderID");
                // @ts-ignore
                var order = this.getView().getModel("datiOrdersModel2").getData();
                var inputField = this.byId("_IDGenText9");
                // @ts-ignore
                var inputValue = inputField.getValue();

                // Aggiorna il campo Quantity nell'oggetto order
                order.Quantity = inputValue;

                const NORTHWIND_URL = "v4/northwind/northwind.svc/";
                var that = this;

                // @ts-ignore
                var jurl = this._ServiceAPI.getServiceUrl().replace(NORTHWIND_URL, "MyEndpoint/updateOrder");

                // @ts-ignore
                return new Promise(
                    // @ts-ignore
                    function (/** @type {Object} */ resolve, reject) {
                        // @ts-ignore
                        $.ajax({
                            method: "PUT",
                            url: jurl,
                            data: JSON.stringify(order),
                            accept: "application/json",
                            contentType: "application/json",
                            async: false,
                        }).done(function (/** @type {Object} */ data) {
                            resolve(data);
                        // @ts-ignore
                        }).fail(function (/** @type {{ responseText: any; }} */ jqXHR, /** @type {any} */ textStatus) {
                            MessageBox.success("Ordine numero " + orderId + " aggiornato correttamente!", {
                                title: "Successo",
                                onClose: function () {
                                    that.onNavBack();
                                }
                            })
                        })
                    }
                )
            },

            onValueHelpRequest: function (oEvent) {
                var oInput = oEvent.getSource();
                var oView = this.getView();
                // Apri il fragment del value help passando l'input corrispondente
                this._openValueHelpFragment(oInput, oView);
            },

            _openValueHelpFragment: function (oInput, oView) {
                var oValueHelpDialog;
                Fragment.load({
                    id: oView.getId(),
                    name: "customer.app1.view.fragments.ValueHelpDialog",
                    controller: this
                }).then(function (oDialog) {
                    oValueHelpDialog = oDialog;
                    oValueHelpDialog._oInput = oInput;
                    oView.addDependent(oValueHelpDialog);
                    oValueHelpDialog.open();
                });
            },

            onValueHelpClose: function (oEvent) {
                var oSelectedItem = oEvent.getParameter("selectedItem");
                if (oSelectedItem) {
                    var oSelectedProduct = oSelectedItem.getBindingContext("productsModel").getObject();
                    var oInput = oEvent.getSource()._oInput;

                    // Aggiorna il valore del ProductID con la selezione del value help
                    oInput.setValue(oSelectedProduct.ProductID);

                    // Aggiorna l'UnitPrice in base alla selezione del value help
                    var oUnitPriceText = oInput.getParent().getCells()[1];
                    oUnitPriceText.setText(oSelectedProduct.UnitPrice + " $");
                }
            },

            readProducts: async function () {
                // @ts-ignore
                var jurl = "v4/northwind/northwind.svc/Products";

                // @ts-ignore
                return new Promise(function (resolve, reject) {
                    // @ts-ignore
                    $.ajax({
                        method: "GET",
                        url: jurl,
                        async: false,
                    }).done(function (data) {
                        this.productsModel.setData(data.value);
                        resolve();
                        // @ts-ignore
                        // @ts-ignore
                    // @ts-ignore
                    }.bind(this)).fail(function (jqHXR, textStatus) {
                        MessageBox.error(jqHXR.responseText);
                        reject();
                    });
                }.bind(this));
            },

            onAddRowPress: function() {
                this.counter++;
                var newInputId = "productInput" + this.counter;
              
                var newRow = new sap.m.ColumnListItem({
                  cells: [
                    // @ts-ignore
                    new sap.m.Input({
                      id: newInputId,
                      placeholder: "seleziona prodotto da aggiungere",
                      showValueHelp: true,
                      valueHelpRequest: this.onValueHelpRequest.bind(this),
                      suggestionItems: {
                        path: "productsModel>/",
                        template: new sap.ui.core.Item({
                          text: "{productsModel>ProductID}"
                        })
                      }
                    }),
                    new sap.m.Text({
                        text: "{productsModel>/UnitPrice}"
                    }),
                    new sap.m.Input({
                      type: sap.m.InputType.Number,
                      liveChange: this.onQuantityChange.bind(this)
                    })
                  ]
                });
              
                // Aggiungi la nuova riga alla tabella
                // @ts-ignore
                this.getView().byId("OrderTable").addItem(newRow);
              },
              
              onDeleteRowPress: function() {
                var oTable = this.getView().byId("OrderTable");
                // @ts-ignore
                var aSelectedItems = oTable.getSelectedItems();
              
                aSelectedItems.forEach(function(oItem) {
                  // @ts-ignore
                  oTable.removeItem(oItem);
                });
              }


        });
    });