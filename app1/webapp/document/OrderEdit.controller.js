sap.ui.define([
    "./BaseController",
    "../model/ServiceAPI",
    "sap/m/MessageBox",
    "sap/ui/core/Fragment",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    // @ts-ignore
    // @ts-ignore
    function (Controller, ServiceAPI, MessageBox, Fragment, Filter, FilterOperator) {
        "use strict";

        return Controller.extend("customer.app1.controller.OrderEdit", {

            counter: 0,

            onInit: function () {

                // @ts-ignore
                this.OrdersModel2 = new sap.ui.model.json.JSONModel();
                // @ts-ignore
                this.getView().setModel(this.OrdersModel2, "OrdersModel2");

                // @ts-ignore
                this.productsModel = new sap.ui.model.json.JSONModel();
                // @ts-ignore
                this.getView().setModel(this.productsModel, "productsModel");

                // @ts-ignore
                this._ServiceAPI = new ServiceAPI(this);

                // @ts-ignore
                this.getRouter().getRoute("RouteOrderDetails").attachPatternMatched(this._handlePagina1Matched, this);

            },

            _handlePagina1Matched: function (evt) {
                var importaParametri = evt.getParameter("arguments")["?query"];
                var indice = importaParametri.OrderID;
                this.readOrderItems(indice);
                this.loadProducts();
            },

            // @ts-ignore
            readOrderItems: async function (indice) {

                // @ts-ignore
                const orderDataMod = await this._ServiceAPI.getOrderItems("Orders(" + indice + ")?$expand=Items");
                var orderItems = this.getView().getModel("OrdersModel2")
                if (orderDataMod) {
                    // @ts-ignore
                    orderItems.setData(orderDataMod);
                } else {
                    // @ts-ignore
                    orderItems.setData([]);
                }
                orderItems.refresh(true);
                // @ts-ignore
                console.log(this.getView().getModel("OrdersModel2").getData());

            },

            loadProducts: async function () {
                // @ts-ignore
                var jurl = "v4/northwind/northwind.svc/Products";

                // @ts-ignore
                return new Promise(function (resolve, reject) {
                    // @ts-ignore
                    $.ajax({
                        method: "GET",
                        url: jurl,
                        async: false,
                    }).done(function (data) {
                        this.productsModel.setData(data.value);
                        resolve();
                        // @ts-ignore
                        // @ts-ignore
                    // @ts-ignore
                    // @ts-ignore
                    }.bind(this)).fail(function (jqHXR, textStatus) {
                        MessageBox.error(jqHXR.responseText);
                        reject();
                    });
                }.bind(this));
            },

            onNavBack: function () {
                // @ts-ignore
                this.goToRoute("RouteView1")
            },

            onValueHelpRequest: function (oEvent) {
                var sInputValue = oEvent.getSource().getValue(),
                    oView = this.getView();

                // @ts-ignore
                if (!this._pValueHelpDialog) {
                    // @ts-ignore
                    this._pValueHelpDialog = Fragment.load({
                        id: oView.getId(),
                        name: "customer.app1.view.fragments.ValueHelpProducts",
                        controller: this
                    }).then(function (oDialog) {
                        oView.addDependent(oDialog);
                        return oDialog;
                    });
                }
                // @ts-ignore
                this._pValueHelpDialog.then(function (oDialog) {
                    //oDialog.getBinding("items").filter([new Filter("ProductName", FilterOperator.Contains, sInputValue)]);
                    oDialog.open(sInputValue);
                })
            },

            onValueHelpClose: function (oEvent) {
                var oSelectedItem = oEvent.getParameter("selectedItem");
                //oEvent.getSource().getBinding("items").filter([]);

                if (!oSelectedItem) {
                    return;
                }

                var oContext = oSelectedItem.getBindingContext("Products");
                var sProductId = oContext.getProperty("ProductID");
                var sProductName = oContext.getProperty("ProductName");
                var sQuantityPerUnit = oContext.getProperty("QuantityPerUnit");
                var sUnitPrice = oContext.getProperty("UnitPrice");

                var oTable = this.getView().byId("orderTable");
                // @ts-ignore
                var aItems = oTable.getItems();
                var oLastRow = aItems[aItems.length - 1];
                var oCells = oLastRow.getCells();
                oCells[0].setValue(sProductId);
                oCells[1].setText(sProductName);
                oCells[2].setText(sQuantityPerUnit);
                oCells[3].setText(sUnitPrice);

                var oModel = new sap.ui.model.json.JSONModel();
                oModel.setData({
                    ProductID: sProductId
                });
                oCells[4].setModel(oModel, "tempModel");
            },

            onAddRowPress: function () {
                this.counter++;
                var newInputId = "productInput" + this.counter;

                // Crea una nuova riga nella tabella
                var newRow = new sap.m.ColumnListItem({
                    cells: [
                        // @ts-ignore
                        new sap.m.Input({
                            id: newInputId,
                            placeholder: "seleziona prodotto da aggiungere",
                            showValueHelp: true,
                            showSuggestion: true,
                            valueHelpRequest: this.onValueHelpRequest.bind(this),
                            suggestionItems: {
                                path: "Products>/",
                                template: new sap.ui.core.Item({
                                    text: "{Products>ProductName}"
                                })
                            }
                        }),
                        new sap.m.Text({
                            text: "{tempModel>/ProductName}"
                        }),
                        new sap.m.Text({
                            text: "{tempModel>/QuantityPerUnit}"
                        }),
                        new sap.m.Text({
                            text: "{tempModel>/UnitPrice}"
                        }),
                        new sap.m.StepInput({
                            min: 0,
                            max: 1000
                        })
                    ]
                });

                // Aggiungi la nuova riga alla tabella
                // @ts-ignore
                this.getView().byId("OrderTable").addItem(newRow);
            },

            onDeleteRowPress: function() {
                var oTable = this.getView().byId("OrderTable");
                // @ts-ignore
                var aSelectedItems = oTable.getSelectedItems();
              
                aSelectedItems.forEach(function(oItem) {
                  // @ts-ignore
                  oTable.removeItem(oItem);
                });
              }



            // @ts-ignore
        });
        // @ts-ignore
    });
