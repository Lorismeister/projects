sap.ui.define([
    "./BaseController",
    "sap/m/MessageBox",
    "sap/ui/core/Fragment",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    'sap/ui/export/library',
    'sap/ui/export/Spreadsheet',
    "sap/ui/model/json/JSONModel",
    "../model/ServiceAPI",
    "../model/formatter"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    // @ts-ignore
    function (Controller, MessageBox, Fragment, Filter, FilterOperator, exportLibrary, Spreadsheet, JSONModel, ServiceAPI, formatter) {
        "use strict";

        return Controller.extend("customer.app1.controller.View1", {
            formatter: formatter,
            counter: 0,
            onInit: function () {

                // @ts-ignore
                this.getRouter().getRoute("RouteView1").attachPatternMatched(this._handleMatched, this);

                // @ts-ignore
                this.OrdersModel = new sap.ui.model.json.JSONModel();
                // @ts-ignore
                this.getView().setModel(this.OrdersModel, "OrdersModel");

                // @ts-ignore
                this.UsersModel = new sap.ui.model.json.JSONModel();
                // @ts-ignore
                this.getView().setModel(this.UsersModel, "UsersModel");

                var Customers = new sap.ui.model.json.JSONModel();
                this.getView().setModel(Customers, "Customers");

                var Products = new sap.ui.model.json.JSONModel();
                this.getView().setModel(Products, "Products");

                var ordersModel = new sap.ui.model.json.JSONModel();
                this.getView().setModel(ordersModel, "ordersModel");

                var aData = [];
                var Prova = new sap.ui.model.json.JSONModel(aData);
                this.getView().setModel(Prova, "Prova");

                // @ts-ignore
                this._ServiceAPI = new ServiceAPI(this);

                // @ts-ignore
                this._oGlobalFilter = null;
                // @ts-ignore
                this._oOrderFilter = null;
                // @ts-ignore
                this._oCustomerFilter = null;

                // @ts-ignore  che cazzo devo mettere al posto dell'url qua sotto? 
                var pModel = new sap.ui.model.json.JSONModel();
                var oView = this.getView();
                // @ts-ignore
                oView.setModel(pModel);
                // @ts-ignore
                this.oSF = oView.byId("ID_OrderID");
                
            },

            _handleMatched: function () {
                this.loadOrders();
                this.loadCustomers();
            },

            loadCustomers: function () {
                var entityName = "Customers";
                var that = this;

                // @ts-ignore
                this._ServiceAPI.getNorthwind(entityName).then(function (data) {
                    // la richiesta ha successo
                    var ordersModel = that.getView().getModel("Customers");
                    // @ts-ignore
                    ordersModel.setData(data.value);

                    // @ts-ignore
                    console.log("Contenuto del modello Customers:", ordersModel.getData());
                }).catch(function (error) {
                    // la richiesta ha fallito
                    MessageBox.error("Impossibile ottenere i dati dell'entità Customers: " + error);
                });
            },

            loadOrders: function () {
                var entityName = "Orders?$expand=Items";

                // @ts-ignore
                this._ServiceAPI.getOrder(entityName).then(function (data) {
                    // la richiesta ha successo
                    var ordersModel = this.getView().getModel("OrdersModel");
                    // @ts-ignore
                    ordersModel.setData(data.value);
                    this.fetchUniqueDataFromOData(data.value);

                    // @ts-ignore
                    console.log("Contenuto del modello Orders:", ordersModel.getData());
                }.bind(this)).catch(function () { //inserire error dentro le parentesi per avere il mex di errore
                    // la richiesta ha fallito
                    //MessageBox.error("Impossibile ottenere i dati dell'entità Orders: " + error);
                });
            },

            // @ts-ignore
            handleRowPress: function (oEvent) {
                var oItem = oEvent.getSource();
                var oCtx = oItem.getBindingContext("OrdersModel");
                var oData = oCtx.getObject();
                var oParams = {
                    OrderID: oData.OrderID,
                    OrderDate: oData.OrderDate,
                    ExpDate: oData.ExpDate,
                    EmployeeID: oData.EmployeeID,
                    CustomerID: oData.CustomerID,
                    ShipName: oData.ShipName
                };
                var oQuery = {
                    query: oParams
                };
                // @ts-ignore
                this.getRouter().navTo("RouteOrderDetails", oQuery);
            },

            onAddPress: function () {
                // @ts-ignore
                this.getRouter().navTo("RouteOrderCreate");

            },

            searchFieldUser: function (evt) {

                var sQuery = evt.getParameter("query");
                var oFilter = null;

                // @ts-ignore
                this._oOrderFilter = null;

                if (sQuery) {
                    // @ts-ignore
                    this._oOrderFilter = new Filter([
                        new Filter("CustomerID", FilterOperator.Contains, sQuery)], false);
                }

                var oFilter = null;


                // @ts-ignore
                if (this._oOrderFilter) {
                    // @ts-ignore
                    oFilter = new Filter([this._oOrderFilter], true);
                    // @ts-ignore
                }
                // @ts-ignore
                this.byId("OrdersTable").getBinding("items").filter(oFilter);

            },

            searchFieldCompanyName: function (evt) {

                var sQuery = evt.getParameter("query");

                // @ts-ignore
                this._oOrderFilter = null;

                if (sQuery) {
                    // @ts-ignore
                    this._oOrderFilter = new Filter([
                        new Filter("CompanyName", FilterOperator.Contains, sQuery)], false);
                }
                var oFilter = null;

                // @ts-ignore
                if (this._oOrderFilter) {
                    // @ts-ignore
                    oFilter = new Filter([this._oOrderFilter], true);
                    // @ts-ignore
                }
                // @ts-ignore
                this.byId("OrdersTable").getBinding("items").filter(oFilter);
            },

            filterGlobally: function (oEvent) {
                var sQuery = oEvent.getParameter("query");
                // @ts-ignore
                this._oGlobalFilter = null;

                if (sQuery) {
                    // @ts-ignore
                    this._oGlobalFilter = new Filter([
                        new Filter("CompanyName", FilterOperator.Contains, sQuery),
                        new Filter("CustomerID", FilterOperator.Contains, sQuery),
                        new Filter("UserName", FilterOperator.Contains, sQuery)
                    ], false);
                }

                this._filter();
            },

            _filter: function () {
                var oFilter = null;

                // @ts-ignore
                if (this._oGlobalFilter) {
                    // @ts-ignore
                    oFilter = new Filter([this._oGlobalFilter], true);
                    // @ts-ignore
                } else if (this._oGlobalFilter) {
                    // @ts-ignore
                    oFilter = this._oGlobalFilter;
                }

                // @ts-ignore
                this.byId("OrdersTable").getBinding("items").filter(oFilter);
            },

            onSuggest: function (event) {
                var sValue = event.getParameter("suggestValue"),
                    aFilters = [];

                console.log(sValue);

                if (sValue) {
                    aFilters = [
                        new Filter([
                            new Filter("OrderID", function (sText) {
                                return (sText || "").toUpperCase().indexOf(sValue.toUpperCase()) > -1;
                            })
                        ], false)
                    ];
                }

                // @ts-ignore
                this.oSF.getBinding("suggestionItems").filter(aFilters);
                // @ts-ignore
                this.oSF.suggest();
            },

            onSort: function () {
                var oTable = this.byId("OrdersTable");
                var oBinding = oTable.getBinding("items");
                var aSorters = [];

                // @ts-ignore
                var sCurrentOrder = oBinding.aSorters[0] ? oBinding.aSorters[0].bDescending : false;

                var bNewOrder = !sCurrentOrder;

                var oSorter = new sap.ui.model.Sorter("OrderID", bNewOrder);

                aSorters.push(oSorter);
                // @ts-ignore
                oBinding.sort(aSorters);

                var oButtonSort = this.byId("sortUsersButton");
                // @ts-ignore
                oButtonSort.setIcon(bNewOrder ? "sap-icon://sort-descending" : "sap-icon://sort-ascending");
            },

            onDelete: function () {
                var Table = this.byId("OrdersTable");
                var Model = this.getView().getModel("OrdersModel");
                // @ts-ignore
                var SelectedItems = Table.getSelectedItems();

                var SelectedID = [];
                var sID = "";

                SelectedItems.forEach(function (oItem) {
                    var sPath = oItem.getBindingContextPath();
                    sID = Model.getProperty(sPath + "/OrderID");
                    SelectedID.push(parseInt(sID));
                });

                var entityName = sID;

                // @ts-ignore
                this._ServiceAPI.getDeleteOrder(entityName);
                this.onReload();
            },

            onReload: function () {
                // window.location.reload(); non serve perché aggiorna inutilmente la pagina
                this.loadOrders(); //In questo caso aggiorno solo la tabella richiamandola
            },


            onEditDialog: function (oEvent) {
                var oTable = this.getView().byId("OrdersTable");
                var oView = this.getView();
                var path = oEvent.getSource().getBindingContext("OrdersModel").getPath();
                var selectedObject = oTable.getModel("OrdersModel").getProperty(path); //getproperty() = indice modello

                var selectedRowModel = new sap.ui.model.json.JSONModel(selectedObject); //tutti i campi
                this.getView().setModel(selectedRowModel, "selectedRowModel");

                if (!this.byId("openDialog")) {
                    Fragment.load({
                        id: oView.getId(),
                        name: "customer.app1.view.fragments.EditDialog",
                        controller: this
                    }).then(function (oDialog) {
                        oView.addDependent(oDialog);
                        oDialog.bindElement({
                            path: path,
                            model: "selectedRowModel"
                        });
                        oDialog.open();
                    });
                }
            },

            closeDialog: function () {
                this.byId("openDialog").destroy();
            },

            updateDialog: function () {

                var that = this;
                var newContext = this.getView().getModel("selectedRowModel").getProperty("/");

                const NORTHWIND_URL = "v4/northwind/northwind.svc/";
                // @ts-ignore
                var jurl = this._ServiceAPI.getServiceURL().replace(NORTHWIND_URL, "MyEndpoint/updateOrder");

                // @ts-ignore
                return new Promise(
                    function (resolve) {
                        // @ts-ignore
                        $.ajax({
                            type: "PUT",
                            data: JSON.stringify(newContext),
                            url: jurl,
                            accept: "application/json",
                            contentType: "application/json",
                            async: false,
                        }).done(function (data) {
                            resolve(data);
                            // @ts-ignore
                            // @ts-ignore
                        }).fail(function (jqXHR, textStatus) {
                            MessageBox.success("L'ordine è stato aggiornato correttamente!", {
                                title: "Aggiornato"
                            })
                            that.onReload();
                        })
                        that.closeDialog();
                    }
                )
            },

            onSearch: function () {
                // @ts-ignore
                var sUserName = this.byId("comboboxFiltraUserName").getValue();
                // @ts-ignore
                var sCustomerID = this.byId("comboboxFiltraCustomerID").getSelectedKey();
                this._applyFilters(sUserName, sCustomerID);
            },

            onReset: function () {
                var oComboBox = this.byId("comboboxFiltraCustomerID");
                // @ts-ignore
                oComboBox.setSelectedKey("");
                var oMultiInput = this.byId("comboboxFiltraUserName");
                // @ts-ignore
                oMultiInput.setValue("");
                this._applyFilters();
            },

            _applyFilters: function (sUserName, sCustomerID) {
                var oTable = this.byId("OrdersTable");
                var oBinding = oTable.getBinding("items");
                var aFilters = [];

                if (sUserName) {
                    var oUserNameFilter = new Filter("UserName", FilterOperator.EQ, sUserName);
                    aFilters.push(oUserNameFilter);
                }

                if (sCustomerID) {
                    var oCustomerIDFilter = new Filter("CustomerID", FilterOperator.EQ, sCustomerID);
                    aFilters.push(oCustomerIDFilter);
                }

                // @ts-ignore
                oBinding.filter(aFilters);
            },

            onExport: function () {
                var oTable = this.getView().byId("OrdersTable");

                // Creazione del modello dati per la tabella
                var oModel = oTable.getModel("OrdersModel");
                var aData = oModel.getProperty("/");

                // Definizione delle colonne per l'esportazione
                var aColumns = [
                    {
                        label: "ORDER ID",
                        property: "OrderID",
                        type: "string"
                    },
                    {
                        label: "ORDER DATE",
                        property: "OrderDate",
                        type: "date"
                    },
                    {
                        label: "USER NAME",
                        property: "UserName",
                        type: "string"
                    },
                    {
                        label: "CUSTOMER ID",
                        property: "CustomerID",
                        type: "string"
                    },
                    {
                        label: "COMPANY NAME",
                        property: "CompanyName",
                        type: "string"
                    }
                ];

                // Creazione dell'oggetto Spreadsheet e impostazione dei dati
                var oSpreadsheet = new Spreadsheet({
                    workbook: {
                        columns: aColumns,
                        fileName: "ListaOrdini.xlsx"
                    },
                    dataSource: aData
                });

                // Download del file Excel
                oSpreadsheet.build();
            },

            fetchUniqueDataFromOData: function (oData) {
                var usersArr = this._reduceArray(oData, "UserName")
                // @ts-ignore
                this.UsersModel.setData(usersArr);
                // @ts-ignore
                // this.UserModel.refresh();
                console.log(UsersModel);

            },

            _reduceArray: function (array, field) {

                var oReduce = array.reduce(function (accumulator, object) {
                    var key = object[field];

                    if (!accumulator.find(c => c === key)) {
                        accumulator.push(object[field]);
                    }
                    return accumulator;

                }, [])
                return oReduce;
            },

            onValueHelpRequest: function(){

            }


            // @ts-ignore
        });
        // @ts-ignore
    });
