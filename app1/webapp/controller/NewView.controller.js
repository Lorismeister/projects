sap.ui.define(
    [
      "./BaseController",  
      "sap/ui/core/mvc/Controller"
    ],
    function(Controller) {
      "use strict";
  
      return Controller.extend("customer.app1.controller.NewView", {
        onInit() {
        },

        onNewPress: function () {
          // @ts-ignore
          this.getRouter().navTo("RouteView1");

      },
        onNewPress2: function () {
          // @ts-ignore
          this.getRouter().navTo("RouteNewCRUDview");

      },
        onNewPress3: function () {
          // @ts-ignore
          this.getRouter().navTo("RouteNuovaCRUD");

      },



      });
    }
  );
  