sap.ui.define([
    "./BaseController",
    "../model/ServiceAPI",
	"../model/formatter",
    "sap/ui/core/routing/History"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, ServiceAPI, formatter, History) {
        "use strict";

        return Controller.extend("customer.app1.controller.OrderDetails", {
            formatter: formatter,
            onInit: function () {

                // @ts-ignore
                this.OrdersModel2 = new sap.ui.model.json.JSONModel();
                // @ts-ignore
                this.getView().setModel(this.OrdersModel2, "OrdersModel2");

                // @ts-ignore
                this._ServiceAPI = new ServiceAPI(this);

                // @ts-ignore
                this.getRouter().getRoute("RouteOrderDetails").attachPatternMatched(this._handlePagina1Matched, this);

            },

            _handlePagina1Matched: function (evt) {
                var importaParametri = evt.getParameter("arguments")["?query"];
                var indice = importaParametri.OrderID;
                this.readOrderItems(indice);
            },

            // @ts-ignore
            readOrderItems: async function (indice) {

                // @ts-ignore
                const orderDataMod = await this._ServiceAPI.getOrderItems("Orders(" + indice + ")?$expand=Items");
                var orderItems = this.getView().getModel("OrdersModel2")
                if (orderDataMod) {
                    // @ts-ignore
                    orderItems.setData(orderDataMod);
                } else {
                    // @ts-ignore
                    orderItems.setData([]);
                }
                orderItems.refresh(true);
                // @ts-ignore
                console.log(this.getView().getModel("OrdersModel2").getData());

            },

            onNavBack: function () {
                var oHistory = History.getInstance();
                var sPreviousHash = oHistory.getPreviousHash();
    
                if (sPreviousHash !== undefined) {
                    window.history.go(-1);
                } else {
                    // @ts-ignore
                    var oRouter = this.getOwnerComponent().getRouter();
                    oRouter.navTo("RouteView1", {}, true);
                }
            }


            // @ts-ignore
        });
        // @ts-ignore
    });
