sap.ui.define(
  [
    "./BaseController",
    "../model/ServiceAPI",
    "sap/ui/core/routing/History",
    "sap/ui/core/Fragment",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "sap/ui/model/Sorter",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/FilterType",
    "sap/ui/model/json/JSONModel"
  ],
  function (Controller, ServiceAPI, History, Fragment, MessageToast, MessageBox, Sorter, Filter,
    FilterOperator, FilterType, JSONModel) {
    "use strict";

    return Controller.extend("customer.app1.controller.NewCRUDview", {
      counter: 0,
      onInit: function () {

        this._ServiceAPI = new ServiceAPI(this);

        var Customers = new sap.ui.model.json.JSONModel();
        this.getView().setModel(Customers, "Customers");

        var Products = new sap.ui.model.json.JSONModel();
        this.getView().setModel(Products, "Products");
        
        var ordersModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(ordersModel, "ordersModel");

        var NewModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(NewModel, "NewModel");

        this.getRouter().getRoute("RouteNewCRUDview").attachPatternMatched(this._handleMatched, this);
      },

      _handleMatched: function () {
        this.loadCustomers();
        this.loadProducts();
      },

      loadCustomers: function () {
        var entityName = "Customers";
        var that = this;

        this._ServiceAPI.getNorthwind(entityName).then(function (data) {
          // la richiesta ha successo
          var ordersModel = that.getView().getModel("Customers");
          ordersModel.setData(data.value);

          console.log("Contenuto del modello Customers:", ordersModel.getData());
        }).catch(function (error) {
          // la richiesta ha fallito
          MessageBox.error("Impossibile ottenere i dati dell'entità Customers: " + error);
        });
      },

      loadProducts: function () {
        var entityName = "Products";
        var that = this;

        this._ServiceAPI.getNorthwind(entityName).then(function (data) {
          // la richiesta ha successo
          var Products = that.getView().getModel("Products");
          Products.setData(data.value);

          console.log("Contenuto del modello Orders:", Products.getData());
        }).catch(function (error) {
          // la richiesta ha fallito
          MessageBox.error("Impossibile ottenere i dati dell'entità Orders: " + error);
        });
      },

      onNavBack: function () {
        var oHistory = History.getInstance();
        var sPreviousHash = oHistory.getPreviousHash();

        if (sPreviousHash !== undefined) {
          window.history.go(-1);
        } else {
          var oRouter = this.getOwnerComponent().getRouter();
          oRouter.navTo("RouteNewView", {}, true);
        }
      },

      onDelete: function () {
        var oSelected = this.byId("myTable").getSelectedIndices();
        for (var i = 0; i < oSelected.length; i++) {
          var oSelectedIndex = oSelected[i];
          var oContext = this.byId("myTable").getContextByIndex(oSelectedIndex);
        }
        oContext.delete().then(function () {
          MessageToast.show("deletionSuccessMessage");
        }.bind(this), function (oError) {
          this._setUIChanges();
          if (oError.canceled) {
            MessageToast.show("deletionRestoredMessage");
            return;
          }
        }.bind(this));
      },

      openCreateDialog: function () {
        var oView = this.getView();

        if (!this._dValueHelpDialog) {
          this._dValueHelpDialog = Fragment.load({
            id: oView.getId(),
            name: "customer.app1.view.fragments.NewCreateDialog",
            controller: this
          }).then(function (oDialog) {
            oView.addDependent(oDialog);
            oDialog.open();
          });
        }
      },

      closeCreateDialog: function () {
        this.byId("openCreateDialog").destroy();
      },

      closeEditDialog: function () {
        this.byId("openEditNewDialog").destroy();
      },

      onValueHelpRequest: function (oEvent) {
        var sInputValue = oEvent.getSource().getValue(),
          oView = this.getView();

        if (!this._pValueHelpDialog) {
          this._pValueHelpDialog = Fragment.load({
            id: oView.getId(),
            name: "customer.app1.view.fragments.NewValueHelpDialog",
            controller: this
          }).then(function (oDialog) {
            oView.addDependent(oDialog);
            return oDialog;
          });
        }
        this._pValueHelpDialog.then(function (oDialog) {
          oDialog.getBinding("items").filter([new Filter("ProductName", FilterOperator.Contains,
            sInputValue)]);
          oDialog.open(sInputValue);
        })
      },

      onValueHelpRequestEdit: function (oEvent) {
        var sInputValue = oEvent.getSource().getValue(),
          oView = this.getView();

        if (!this._pValueHelpDialog) {
          this._pValueHelpDialog = Fragment.load({
            id: oView.getId(),
            name: "customer.app1.view.fragments.NewValueHelpDialogEdit",
            controller: this
          }).then(function (oDialog) {
            oView.addDependent(oDialog);
            return oDialog;
          });
        }
        this._pValueHelpDialog.then(function (oDialog) {
          oDialog.getBinding("items").filter([new Filter("OrderID", FilterOperator.Contains,
            sInputValue)]);
          oDialog.open(sInputValue);
        })
      },

      onValueHelpSearch: function (oEvent) {
        var sValue = oEvent.getParameter("value");
        var oFilter = new Filter("ProductName", FilterOperator.Contains, sValue);

        oEvent.getSource().getBinding("items").filter([oFilter]);
      },

      onValueHelpClose: function (oEvent) {
        var oSelectedItem = oEvent.getParameter("selectedItem");
        oEvent.getSource().getBinding("items").filter([]);

        if (!oSelectedItem) {
          return;
        }

        var oContext = oSelectedItem.getBindingContext("Products");
        var sProductId = oContext.getProperty("ProductID");
        var sProductName = oContext.getProperty("ProductName");
        var sQuantityPerUnit = oContext.getProperty("QuantityPerUnit");
        var sUnitPrice = oContext.getProperty("UnitPrice");

        var oTable = this.getView().byId("MyOrderTable");
        var aItems = oTable.getItems();
        var oLastRow = aItems[aItems.length - 1];
        var oCells = oLastRow.getCells();
        oCells[0].setValue(sProductId);
        oCells[1].setText(sProductName);
        oCells[2].setText(sQuantityPerUnit);
        oCells[3].setText(sUnitPrice);

        var oModel = new sap.ui.model.json.JSONModel();
        oModel.setData({
          ProductID: sProductId
        });
        oCells[4].setModel(oModel, "tempModel");
      },

      onValueHelpCloseEdit: function (oEvent) {
        var oSelectedItem0 = oEvent.getParameter("selectedItem");
        oEvent.getSource().getBinding("items").filter([]);

        if (!oSelectedItem0) {
          return;
        }

        this.byId("OrderID").setValue(oSelectedItem0.getTitle());
      },

      onValueHelpSearchEdit: function (oEvent) {
        var sValue = oEvent.getParameter("value");
        var oFilter = new Filter("OrderID", FilterOperator.Contains, sValue);

        oEvent.getSource().getBinding("items").filter([oFilter]);
      },

      onValueCustomerHelpRequest: function (oEvent) {
        var sInputValue2 = oEvent.getSource().getValue(),
          oView2 = this.getView();

        if (!this._pValueHelpDialog2) {
          this._pValueHelpDialog2 = Fragment.load({
            id: oView2.getId(),
            name: "customer.app1.view.fragments.NewValueCustomersHelpDialog",
            controller: this
          }).then(function (oDialog2) {
            oView2.addDependent(oDialog2);
            return oDialog2;
          });
        }
        this._pValueHelpDialog2.then(function (oDialog2) {
          oDialog2.getBinding("items").filter([new Filter("CustomerID", FilterOperator.Contains,
            sInputValue2)]);
          oDialog2.open(sInputValue2);
        })
      },

      onValueCustomerHelpRequestEdit: function (oEvent) {
        var sInputValue3 = oEvent.getSource().getValue(),
          oView2 = this.getView();

        if (!this._pValueHelpDialog2) {
          this._pValueHelpDialog2 = Fragment.load({
            id: oView2.getId(),
            name: "customer.app1.view.fragments.NewValueHelpDialogEdit",
            controller: this
          }).then(function (oDialog2) {
            oView2.addDependent(oDialog2);
            return oDialog2;
          });
        }
        this._pValueHelpDialog2.then(function (oDialog2) {
          oDialog2.getBinding("items").filter([new Filter("OrderID", FilterOperator.Contains,
            sInputValue3)]);
          oDialog2.open(sInputValue3);
        })
      },

      onValueHelpSearchCustomerID: function (oEvent) {
        var sValue2 = oEvent.getParameter("value");
        var oFilter2 = new Filter("CustomerID", FilterOperator.Contains, sValue2);

        oEvent.getSource().getBinding("items").filter([oFilter2]);
      },

      onValueCloseCustomerID: function (oEvent) {
        var oSelectedItem2 = oEvent.getParameter("selectedItem");
        oEvent.getSource().getBinding("items").filter([]);

        if (!oSelectedItem2) {
          return;
        }

        this.byId("CustomerInput").setValue(oSelectedItem2.getTitle());
        //  this.getView().byId("CustomerInput").destroyItems();
      },

      onValueCompanyNameHelpRequest: function (oEvent) {
        var sInputValue3 = oEvent.getSource().getValue(),
          oView2 = this.getView();

        if (!this._pValueHelpDialog3) {
          this._pValueHelpDialog3 = Fragment.load({
            id: oView2.getId(),
            name: "customer.app1.view.fragments.NewValueCompanyNameHelpDialog",
            controller: this
          }).then(function (oDialog3) {
            oView2.addDependent(oDialog3);
            return oDialog3;
          });
        }
        this._pValueHelpDialog3.then(function (oDialog3) {
          oDialog3.getBinding("items").filter([new Filter("CompanyName", FilterOperator.Contains,
            sInputValue3)]);
          oDialog3.open(sInputValue3);
        })
      },

      onValueSearchCompanyName: function (oEvent) {
        var sValue3 = oEvent.getParameter("value");
        var oFilter3 = new Filter("CompanyName", FilterOperator.Contains, sValue3);

        oEvent.getSource().getBinding("items").filter([oFilter3]);
      },

      onValueCloseCompanyName: function (oEvent) {
        var oSelectedItem3 = oEvent.getParameter("selectedItem");
        oEvent.getSource().getBinding("items").filter([]);

        if (!oSelectedItem3) {
          return;
        }

        this.byId("CompanyNameInput").setValue(oSelectedItem3.getTitle());
        //this.getView().byId("CustomerInput").destroyItems();
      },

      onAddOrderPress: function () {
        var customerId = this.getView().byId("CustomerInput").getValue();
        var companyName = this.getView().byId("CompanyNameInput").getValue();
        var userName = "lorismastroianni97@gmail.com";
        var today = new Date();
        var todayFormatted = today.toISOString();

        //var oneYear = new Date("2025-12-31");

        var oneYear = new Date();
        var year = today.getFullYear() + 1;
        oneYear.setFullYear(year);

        //var orderId = 1;

        // Recupera l'ultimo OrderID dal ordersModel
        /**var ordersModello = this.getView().getModel("MyModel");
        var path = 
        var orders = ordersModello.getProperty("/");
        var lastOrderId = orders[orders.length - 1].OrderID;
        var orderId = lastOrderId + 1;*/
        var orderId = 101;

        if (!customerId || !companyName) {
          // Almeno uno dei campi obbligatori è vuoto, mostra un messaggio di errore
          if (customerId == "") {
            this.getView().byId("CustomerInput").setValueState("Error");
          } else this.getView().byId("CustomerInput").setValueState("None");
          if (companyName == "") {
            this.getView().byId("CompanyNameInput").setValueState("Error");
          } else this.getView().byId("CompanyNameInput").setValueState("None");
          MessageBox.error("ATTENZIONE! Non stai selezionando il CustomerID!");
          return;
        }

        // Recupera i valori dalla tabella
        var tableItems = this.getView().byId("MyOrderTable").getItems();
        var orderItems = [];
        tableItems.forEach(function (tableItem, index) {
          var productId = parseInt(tableItem.getCells()[0].getValue(), 20);
          var productName = tableItem.getCells()[1].getText();
          var quantityPerUnit = tableItem.getCells()[2].getText();
          var unitPrice = parseInt(tableItem.getCells()[3].getText(), 10);
          var quantity = tableItem.getCells()[4].getValue();

          if (!productId || !productName) {
            // Almeno uno dei campi obbligatori è vuoto, mostra un messaggio di errore
            MessageBox.error("Si prega di selezionare un ID Prodotto");
            return this.onAddOrderPress();
          }

          if (quantity == 0) {
            // Almeno uno dei campi obbligatori è vuoto, mostra un messaggio di errore
            MessageBox.error("Si prega di inserire almeno un'unità nella casella quantità.");
            return this.onAddOrderPress();
          }

          //Index mi serve a far partire da 1 e poi incrementa di 1 

          var orderItem = {
            Order: index + 1,
            ProductID: productId,
            ProductName: productName,
            QuantityPerUnit: quantityPerUnit,
            UnitPrice: unitPrice,
            Quantity: quantity

          };

          orderItems.push(orderItem);
        });

        var order = {
          OrderID: orderId,
          OrderDate: todayFormatted,
          ExpDate: oneYear,
          UserName: userName,
          CustomerID: customerId,
          CompanyName: companyName,
          Items: orderItems,

        };
        
        console.log(order);

        this.onCreate(order);

        MessageBox.success("Ordine inviato con successo. Numero d'ordine: " + orderId, {
          onClose: function () {
            this.byId("openCreateDialog").destroy();
            window.location.reload();
          }.bind(this)
        });
        
      },

      onCreate: function (order) {
        var oModel = new sap.ui.model.json.JSONModel();
        oModel.setData(order);

        var pModel = this.getView().getModel("MyModel");
        var oListBinding = pModel.bindList("/Orders");
        var oContext = oListBinding.create(order);
        oContext.created().then(
          function () {
            //success
          }.bind(this),
          function () {
            //error
          }.bind(this)
        )
      },

      onEditDialog: function () {
        var oView = this.getView();

        if (!this._dValueHelpDialog) {
          this._dValueHelpDialog = Fragment.load({
            id: oView.getId(),
            name: "customer.app1.view.fragments.NewEditDialog",
            controller: this
          }).then(function (oDialog) {
            oView.addDependent(oDialog);
            oDialog.open();
          });
        }
      }
      
    });
  }
);
      
/**
      onEditDialogghe: function (oEvent) {
        var oItem = oEvent.getSource();
                var oCtx = oItem.getBindingContext("MyModel");
                var oData = oCtx.getObject();
                var oParams = {
                    OrderID: oData.OrderID,
                    OrderDate: oData.OrderDate,
                    ExpDate: oData.ExpDate,
                    EmployeeID: oData.EmployeeID,
                    CustomerID: oData.CustomerID,
                    ShipName: oData.ShipName
                };
        var editModel = new sap.ui.model.json.JSONModel();
        editModel.setData(oParams);
        
        
        var oBinding = this.byId("myTable").getBinding("rows");
        var oView = this.getView();

        var oSelected = this.byId("myTable").getSelectedIndices();
        for (var i = 0; i < oSelected.length; i++) {
          var oSelectedIndex = oSelected[i];
          var oContext = this.byId("myTable").getContextByIndex(oSelectedIndex);
        }

        var oTable = this.getView().byId("myTable");
        var oView = this.getView();
        var path = oEvent.getSource().getBindingContext("MyModel").getPath();
        var selectedObject = oTable.getModel("MyModel").getProperty(path); //getproperty() = indice modello

        var selectedRowModel = new sap.ui.model.json.JSONModel(selectedObject); //tutti i campi
        this.getView().setModel(selectedRowModel, "selectedRowModel");

        if (!this.byId("openEditNewDialog")) {
          Fragment.load({
            id: oView.getId(),
            name: "customer.app1.view.fragments.NewEditDialog",
            controller: this
          }).then(function (oDialog) {
            oView.addDependent(oDialog);
            oDialog.bindElement({
              //path: path,
              model: "selectedRowModel"
            });
            oDialog.open();
          });
        }
      },

      openEditDialog: function (oEvent) {
        var oView = this.getView();
        var oSelected = this.byId("myTable").getSelectedIndices();       

        for (var i = 0; i < oSelected.length; i++) {
          var oSelectedIndex = oSelected[i];
          var oContext = this.byId("myTable").getContextByIndex(oSelectedIndex);
        }

        var selectedRowModel = new sap.ui.model.json.JSONModel(oContext); //tutti i campi
        this.getView().setModel(selectedRowModel, "selectedRowModel");

        var path = oEvent.getSource().getBindingContext("MyModel").getPath();

        if (!this.byId("openEditNewDialog")) {
          Fragment.load({
            id: oView.getId(),
            name: "customer.app1.view.fragments.NewEditDialog",
            controller: this
          }).then(function (oDialog) {
            oView.addDependent(oDialog);
            oDialog.bindElement({
              path: path,
              model: "selectedRowModel"
            });
            oDialog.open();
          });
        }
      },

      closeDialog: function () {
        this.byId("openDialog").destroy();
      },


      onUpdate: function (order) {
        var oModel = new sap.ui.model.json.JSONModel();
        oModel.setData(order);

        var pModel = this.getView().getModel("MyModel");
        var oListBinding = pModel.bindList("/Orders");
        var oContext = oListBinding.update(order);
        oContext.updated().then(
          function () {
            //success
          }.bind(this),
          function () {
            //error
          }.bind(this)
        )
      },*/


   


/**oSelected non è un indice ma un'array di indici quindi lo bisogna ciclare 
         in modo che al posto di "indice" passi una variabile che volta per volta 
         mi faccia il getContextByIndex sul rispettivo indice*/