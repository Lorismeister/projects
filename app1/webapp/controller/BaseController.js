sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/BusyIndicator",
    "sap/m/MessageBox",
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, BusyIndicator, MessageBox) {
        "use strict";

        return Controller.extend("customer.app1.controller.BaseController", {
            onInit: function () {

            },

            getRouter: function () {
                return sap.ui.core.UIComponent.getRouterFor(this);
            },

            onNavBack: function () {
                history.go(-1);
            },

            goToRoute: function (route, params, replaceHash) {
                if (!route) {
                    route = "";
                }
                if (!params) {
                    params = {};
                }

                if (typeof replaceHash !== 'boolean') {
                    replaceHash = true;
                }
                // @ts-ignore
                this.getRouter().navTo(route, params, replaceHash);
            },

            oDataRead: function (sURL, sEntitySet, oFilter) {
                // @ts-ignore
                var oModel = new sap.ui.model.odata.v2.ODataModel(sURL, true);
                // @ts-ignore
                return new Promise(function (resolve, reject) {
                    oModel.read(sEntitySet, {
                        // @ts-ignore
                        async: false,
                        filters: [oFilter],
                        success: function (oData) {
                            resolve(oData);
                        },
                        error: function (e) {
                            MessageBox.error("Backend data read error.");
                            reject(e);
                            BusyIndicator.hide();
                        },
                    });
                });
            },
        });
    });