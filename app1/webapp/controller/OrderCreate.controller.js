sap.ui.define([
    "./BaseController",
    "../model/ServiceAPI",
    "sap/m/MessageBox",
    "sap/ui/core/Fragment",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/core/routing/History"
],

    function (Controller, ServiceAPI, MessageBox, Fragment, Filter, FilterOperator, History) {
        "use strict";

        return Controller.extend("customer.app1.controller.OrderCreate", {
            counter: 0,
            onInit: function () {

                this._ServiceAPI = new ServiceAPI(this);

                var Customers = new sap.ui.model.json.JSONModel();
                this.getView().setModel(Customers, "Customers");

                var Products = new sap.ui.model.json.JSONModel();
                this.getView().setModel(Products, "Products");

                var ordersModel = new sap.ui.model.json.JSONModel();
                this.getView().setModel(ordersModel, "ordersModel");

                var aData = [];
                var Prova = new sap.ui.model.json.JSONModel(aData);
                this.getView().setModel(Prova, "Prova");

                this.getRouter().getRoute("RouteOrderCreate").attachPatternMatched(this._handleMatched, this);

            },

            _handleMatched: function () {

                this.loadCustomers();
                this.loadProducts();
                this.loadOrders();
            },

            loadCustomers: function () {
                var entityName = "Customers";
                var that = this;

                this._ServiceAPI.getNorthwind(entityName).then(function (data) {
                    // la richiesta ha successo
                    var ordersModel = that.getView().getModel("Customers");
                    ordersModel.setData(data.value);

                    console.log("Contenuto del modello Customers:", ordersModel.getData());
                }).catch(function (error) {
                    // la richiesta ha fallito
                    MessageBox.error("Impossibile ottenere i dati dell'entità Customers: " + error);
                });
            },

            loadProducts: function () {
                var entityName = "Products";
                var that = this;

                this._ServiceAPI.getNorthwind(entityName).then(function (data) {
                    // la richiesta ha successo
                    var Products = that.getView().getModel("Products");
                    Products.setData(data.value);

                    console.log("Contenuto del modello Orders:", Products.getData());
                }).catch(function (error) {
                    // la richiesta ha fallito
                    MessageBox.error("Impossibile ottenere i dati dell'entità Orders: " + error);
                });
            },

            loadOrders: function () {
                var entityName = "Orders";
                var that = this;

                this._ServiceAPI.getOrderItems(entityName).then(function (data) {
                    // la richiesta ha successo
                    var ordersModel = that.getView().getModel("ordersModel");
                    ordersModel.setData(data.value);

                    console.log("Contenuto del modello Orders:", ordersModel.getData());
                }).catch(function (error) {
                    // la richiesta ha fallito
                    MessageBox.error("Impossibile ottenere i dati dell'entità Orders: " + error);
                });
            },

            onAddRowPress: function () {
                this.counter++;
                var newInputId = "productInput" + this.counter;

                // Crea una nuova riga nella tabella
                var newRow = new sap.m.ColumnListItem({
                    cells: [
                        // @ts-ignore
                        new sap.m.Input({
                            id: newInputId,
                            placeholder: "seleziona prodotto da aggiungere",
                            showValueHelp: true,
                            showSuggestion: true,
                            valueHelpRequest: this.onValueHelpRequest.bind(this),
                            suggestionItems: {
                                path: "Products>/",
                                template: new sap.ui.core.Item({
                                    text: "{Products>ProductName}"
                                })
                            }
                        }),
                        new sap.m.Text({
                            text: "{tempModel>/ProductName}"
                        }),
                        new sap.m.Text({
                            text: "{tempModel>/QuantityPerUnit}"
                        }),
                        new sap.m.Text({
                            text: "{tempModel>/UnitPrice}"
                        }),
                        new sap.m.StepInput({
                            min: 0,
                            max: 1000
                        }),
                        new sap.m.Button({
                            icon: "sap-icon://delete",
                            press: function (oEvent) {
                                var oTable = this.getView().byId("orderTable");
                                var oItemToDelete = oEvent.getSource().getParent();
                                oTable.removeItem(oItemToDelete);
                            }.bind(this)
                        })
                    ]
                });

                // Aggiungi la nuova riga alla tabella
                this.getView().byId("orderTable").addItem(newRow);
            },

            onValueHelpRequest: function (oEvent) {
                var sInputValue = oEvent.getSource().getValue(),
                    oView = this.getView();

                if (!this._pValueHelpDialog) {
                    this._pValueHelpDialog = Fragment.load({
                        id: oView.getId(),
                        name: "customer.app1.view.fragments.ValueHelpDialog",
                        controller: this
                    }).then(function (oDialog) {
                        oView.addDependent(oDialog);
                        return oDialog;
                    });
                }
                this._pValueHelpDialog.then(function (oDialog) {
                    oDialog.getBinding("items").filter([new Filter("ProductName", FilterOperator.Contains, sInputValue)]);
                    oDialog.open(sInputValue);
                })
            },

            onValueHelpSearch: function (oEvent) {
                var sValue = oEvent.getParameter("value");
                var oFilter = new Filter("ProductName", FilterOperator.Contains, sValue);

                oEvent.getSource().getBinding("items").filter([oFilter]);
            },

            onValueHelpClose: function (oEvent) {
                var oSelectedItem = oEvent.getParameter("selectedItem");
                oEvent.getSource().getBinding("items").filter([]);

                if (!oSelectedItem) {
                    return;
                }

                var oContext = oSelectedItem.getBindingContext("Products");
                var sProductId = oContext.getProperty("ProductID");
                var sProductName = oContext.getProperty("ProductName");
                var sQuantityPerUnit = oContext.getProperty("QuantityPerUnit");
                var sUnitPrice = oContext.getProperty("UnitPrice");

                var oTable = this.getView().byId("orderTable");
                var aItems = oTable.getItems();
                var oLastRow = aItems[aItems.length - 1];
                var oCells = oLastRow.getCells();
                oCells[0].setValue(sProductId);
                oCells[1].setText(sProductName);
                oCells[2].setText(sQuantityPerUnit);
                oCells[3].setText(sUnitPrice);

                var oModel = new sap.ui.model.json.JSONModel();
                oModel.setData({
                    ProductID: sProductId
                });
                oCells[4].setModel(oModel, "tempModel");
            },

            onValueCustomerHelpRequest: function (oEvent) {
                var sInputValue2 = oEvent.getSource().getValue(),
                    oView2 = this.getView();

                if (!this._pValueHelpDialog2) {
                    this._pValueHelpDialog2 = Fragment.load({
                        id: oView2.getId(),
                        name: "customer.app1.view.fragments.ValueCustomersHelpDialog",
                        controller: this
                    }).then(function (oDialog2) {
                        oView2.addDependent(oDialog2);
                        return oDialog2;
                    });
                }
                this._pValueHelpDialog2.then(function (oDialog2) {
                    oDialog2.getBinding("items").filter([new Filter("CustomerID", FilterOperator.Contains, sInputValue2)]);
                    oDialog2.open(sInputValue2);
                })
            },
            
            onValueHelpSearchCustomerID: function (oEvent) {
                var sValue2 = oEvent.getParameter("value");
                var oFilter2 = new Filter("CustomerID", FilterOperator.Contains, sValue2);

                oEvent.getSource().getBinding("items").filter([oFilter2]);
            },

            onValueCloseCustomerID: function (oEvent) {
                var oSelectedItem2 = oEvent.getParameter("selectedItem");
                oEvent.getSource().getBinding("items").filter([]);

                if (!oSelectedItem2) {
                    return;
                }
                
                this.byId("CustomerInput").setValue(oSelectedItem2.getTitle());
              //  this.getView().byId("CustomerInput").destroyItems();
            },

            onValueCompanyNameHelpRequest: function (oEvent) {
                var sInputValue3 = oEvent.getSource().getValue(),
                    oView2 = this.getView();

                if (!this._pValueHelpDialog3) {
                    this._pValueHelpDialog3 = Fragment.load({
                        id: oView2.getId(),
                        name: "customer.app1.view.fragments.ValueCompanyNameHelpDialog",
                        controller: this
                    }).then(function (oDialog3) {
                        oView2.addDependent(oDialog3);
                        return oDialog3;
                    });
                }
                this._pValueHelpDialog3.then(function (oDialog3) {
                    oDialog3.getBinding("items").filter([new Filter("CompanyName", FilterOperator.Contains, sInputValue3)]);
                    oDialog3.open(sInputValue3);
                })
            },

            onValueSearchCompanyName: function (oEvent) {
                var sValue3 = oEvent.getParameter("value");
                var oFilter3 = new Filter("CompanyName", FilterOperator.Contains, sValue3);

                oEvent.getSource().getBinding("items").filter([oFilter3]);
            },

            onValueCloseCompanyName: function (oEvent) {
                var oSelectedItem3 = oEvent.getParameter("selectedItem");
                oEvent.getSource().getBinding("items").filter([]);

                if (!oSelectedItem3) {
                    return;
                }
                
                this.byId("CompanyNameInput").setValue(oSelectedItem3.getTitle());
                //this.getView().byId("CustomerInput").destroyItems();
            },

            onAddOrderPress: function () {
                var customerId = this.getView().byId("CustomerInput").getValue();
                var companyName = this.getView().byId("CompanyNameInput").getValue();
                var userName = "lorismastroianni97@gmail.com";
                var today = new Date();
                var todayFormatted = today.toISOString();

                //var oneYear = new Date("2025-12-31");

                var oneYear = new Date();
                var year = today.getFullYear() + 1;
                oneYear.setFullYear(year);

                // Recupera l'ultimo OrderID dal ordersModel
                var orders = this.getView().getModel("ordersModel").getProperty("/");
                var lastOrderId = orders[orders.length - 1].OrderID;
                var orderId = lastOrderId + 1;

                if (!customerId || !companyName) {
                    // Almeno uno dei campi obbligatori è vuoto, mostra un messaggio di errore
                    if (customerId == "") {
                        this.getView().byId("CustomerInput").setValueState("Error");
                    } else this.getView().byId("CustomerInput").setValueState("None");
                    if (companyName == ""){ 
                        this.getView().byId("CompanyNameInput").setValueState("Error");
                    } else this.getView().byId("CompanyNameInput").setValueState("None");
                    MessageBox.error("ATTENZIONE! Non stai selezionando il CustomerID!");
                    return;
                  }

                // Recupera i valori dalla tabella
                var tableItems = this.getView().byId("orderTable").getItems();
                var orderItems = [];
                tableItems.forEach(function (tableItem, index) {
                    var productId = parseInt(tableItem.getCells()[0].getValue(), 20);
                    var productName = tableItem.getCells()[1].getText();
                    var quantityPerUnit = tableItem.getCells()[2].getText();
                    var unitPrice = parseInt(tableItem.getCells()[3].getText(), 10);
                    var quantity = tableItem.getCells()[4].getValue();

                    if (!productId || !productName) {
                        // Almeno uno dei campi obbligatori è vuoto, mostra un messaggio di errore
                        MessageBox.error("Si prega di selezionare un ID Prodotto");
                        return this.onAddOrderPress();
                      } 

                    if (quantity == 0) {
                        // Almeno uno dei campi obbligatori è vuoto, mostra un messaggio di errore
                        MessageBox.error("Si prega di inserire almeno un'unità nella casella quantità.");
                        return this.onAddOrderPress();
                      }

                    //Index mi serve a far partire da 1 e poi incrementa di 1 

                    var orderItem = {
                        Order: index + 1,
                        ProductID: productId,
                        ProductName: productName,
                        QuantityPerUnit: quantityPerUnit,
                        UnitPrice: unitPrice,
                        Quantity: quantity

                    };

                    orderItems.push(orderItem);
                });

                var order = {
                    OrderID: orderId,
                    OrderDate: todayFormatted,
                    ExpDate: oneYear,
                    UserName: userName,
                    CustomerID: customerId,
                    CompanyName: companyName,
                    Items: orderItems,

                };

                console.log(order);

                this.onSave(order);

                MessageBox.success("Ordine inviato con successo. Numero d'ordine: " + orderId, {
                    onClose: function () {
                        this.getView().byId("orderTable").destroyItems();
                        this.goToRoute("RouteView1")
                    }.bind(this)
                });
            },

            onSave: function (order) {
                // var ord = this.getView().getModel("orderItems");
                this._ServiceAPI.getNewSaveOrder(order)
            },

            onDeleteRowPress: function () {
                var table = this.getView().byId("orderTable");
                var selectedRow = table.getSelectedItem();
                if (selectedRow) {
                    table.removeItem(selectedRow);
                }
            },

            onNavBack: function () {
                var oHistory = History.getInstance();
                var sPreviousHash = oHistory.getPreviousHash();
    
                if (sPreviousHash !== undefined) {
                    window.history.go(-1);
                } else {
                    var oRouter = this.getOwnerComponent().getRouter();
                    oRouter.navTo("RouteView1", {}, true);
                }
            },

            recuperaModello: function() {
                var recoverModel = new sap.ui.model.json.JSONModel();
                this.getView().setModel(recoverModel, "recoverModel");


            }

        })
    })