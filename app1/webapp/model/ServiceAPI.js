sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
],

    function (UIComponent, MessageToast, MessageBox) {
        "use strict";

        const NORTHWIND_URL = "v4/northwind/northwind.svc/";

        return UIComponent.extend("customer.app1.model.ServiceAPI", {
            /**
             * 
             * @param {*} oCtrl 
             */
            constructor: function (oCtrl) {
                this._oCtrl = oCtrl;
            },

            getServiceURL: function () {
                var contr = this._oCtrl.getOwnerComponent().getModel().sServiceUrl;
                return contr
            },

            /**
             * return data from MyEndpoint service
             * @param {*} serviceName 
             */

            GetMyendpoint: function (serviceName) {

                var jurl = this.getserviceURL().replace(NORTHWIND_URL, "MyEndpoint/" + serviceName);
                // @ts-ignore
                $.get(jurl).done(function (data) {
                    MessageToast.show(data);
                });

            },

            getMyCustomers: function (serviceName) {
                var jurl = this.getServiceURL().replace(NORTHWIND_URL, "Customers" + serviceName);
                // @ts-ignore
                /**       $.get(jurl).done(function( data ) {
                           MessageToast.show(data);
                         });
                         */
                // @ts-ignore
                return new Promise(
                    function (/** @type {Object} */ resolve) {
                        // @ts-ignore
                        $.ajax({
                            method: "GET",
                            url: jurl,
                            async: false,
                        }).done(function (/** @type {Object} */ data) {
                            resolve(data);
                            // @ts-ignore
                        }).fail(function (/** @type {{ responseText: any; }} */ jqXHR, /** @type {any} */ textStatus) {
                            MessageBox.error(jqXHR.responseText);
                        });
                    });
            },

            getNorthwind: function (entityName) {
                var jurl = this.getServiceURL() + entityName;
                // @ts-ignore
                return new Promise(function (resolve, reject) {
                    // @ts-ignore
                    $.ajax({
                        method: "GET",
                        url: jurl,
                        async: false,
                    }).done(function (data) {
                        resolve(data);
                        // @ts-ignore
                    }).fail(function (jqXHR, textStatus) {
                        reject(jqXHR.responseText);
                    });
                });
            },

            getOrder: function (entityName) {
                //var jurl = this.getServiceURL().replace(NORTHWIND_URL, "odata/v4/CatalogService/" + entityName);
                var jurl = "odata/v4/CatalogService/" + entityName;
                // @ts-ignore
                return new Promise(function (resolve, reject) {
                    // @ts-ignore
                    $.ajax({
                        method: "GET",
                        url: jurl,
                        async: false,
                    }).done(function (data) {
                        resolve(data);
                        // @ts-ignore
                    }).fail(function (jqXHR, textStatus) {
                        reject(jqXHR.responseText);
                    });
                });
            },

            getOrderItems: function (entityName) {
                var jurl = this.getServiceURL().replace(NORTHWIND_URL, "odata/v4/CatalogService/" + entityName);
                //jurl += "?$filter=OrderID eq" + OrderID;
                // @ts-ignore
                return new Promise(function (resolve, reject) {
                    // @ts-ignore
                    $.ajax({
                        method: "GET",
                        url: jurl,
                        async: false,
                    }).done(function (data) {
                        resolve(data);
                        // @ts-ignore
                    }).fail(function (jqXHR, textStatus) {
                        reject(jqXHR.responseText);
                    });
                });
            },

            getNewSaveOrder: function (order) {
                var jurl = this.getServiceURL().replace(NORTHWIND_URL, "MyEndpoint/saveOrder");
                // @ts-ignore
                return new Promise(
                    // @ts-ignore
                    function (resolve, reject) {
                        // @ts-ignore
                        $.ajax({
                            method: "POST",
                            url: jurl,
                            data: JSON.stringify(order),
                            contentType: "application/json",
                            async: true,
                        }).done(function (data) {
                            resolve(data);
                            // @ts-ignore
                        }).fail(function (jqXHR, textStatus) {
                            MessageBox.error(jqXHR.responseText)
                        });
                    });
            },

            getUpdateOrder: function (order) {
                var jurl = this.getServiceURL().replace(NORTHWIND_URL, "MyEndpoint/updateOrder");
                // @ts-ignore
                return new Promise(
                    // @ts-ignore
                    function (resolve, reject) {
                        // @ts-ignore
                        $.ajax({
                            method: "PUT",
                            url: jurl,
                            data: JSON.stringify(order),
                            accept: "application/json",
                            contentType: "application/json",
                            async: true,
                        }).fail(function (data) {
                            resolve(data);
                            // @ts-ignore
                        }).done(function (jqXHR, textStatus) {
                            MessageBox.success("L'ordine n° " + order + " è stato aggiornato correttamente!", {
                                title: "Aggiornato"
                            })
                        });
                    });
            },

            getDeleteOrder: function (entityName) {
                 var jurl = this.getServiceURL().replace(NORTHWIND_URL, "odata/v4/CatalogService/Orders" + "("+ entityName + ")");
                 // @ts-ignore
                 return new Promise(
                     // @ts-ignore
                     function (resolve, reject) {
                         // @ts-ignore
                         $.ajax({
                             method: "DELETE",
                             url: jurl,
                             data: JSON.stringify(entityName),
                             accept: "application/json",
                             contentType: "application/json",
                             async: false,
                         }).done(function (data) {
                             resolve(data);
                         // @ts-ignore
                         }).fail(function (jqXHR, textStatus) {
                             // @ts-ignore
                             MessageBox.success("L'ordine n° " + entityName + " è stato eliminato correttamente!", {
                             // @ts-ignore
                             title: "Successo",
                             // @ts-ignore
                             /**onClose: function() {
                                that.onReload();
                                }*/
                             })
                         });
                     });
             },
             
             onReload: function () {
                this.loadOrders(); 
            },
        }
        );
    });