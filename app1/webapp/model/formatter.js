sap.ui.define([
	"sap/ca/ui/model/format/DateFormat"
], 

function (DateFormat) {
	"use strict";
	return {
		statusText: function (sStatus) {
			//sStatus = 0; Se vuoi verificare se mette il trattino
			var sReplace = "";
			
			if (!sStatus || sStatus == 0) { // sStatus is 0 (truthy and falsy)
	  
			  sReplace = " - ";      
	  
			} else {
	  
			  sReplace = sStatus + " €";
	  
			}
			return sReplace; 
		},

		statusDate: function (dStatus){
			var dateForm = dStatus.split("-");
			var t = dateForm[2].split("T")[0]
			var date = dateForm[0] + "/" + dateForm[1] + "/" + t;
			return date;
		},

		DateFormatter: function (fdStatus){
			var date = new Date(fdStatus);
			var year = date.getFullYear();
			var month = date.getMonth();
			var day = date.getDate();

			var newDate = year + "/" + month + "/" + day
			return newDate;
		}
	};
});

/** trycatch ---------- {pattern :"dd.MM.yyyy"}
fooBar: function(iValue) {

      var sReturn = "";
      
      if (sStatus == 0) { // sStatus is 0 (truthy and falsy)

        sRetrun = "null";      

      } else {

        sReturn = sStatus + "€";

      } else if (iValue > 5) {

        sReturn = "foo bar";

      }

      // return sReturn to the view
      return sReturn;    

    } */